"""Handle onedrive export.

(2021-09-20) Initial version
"""

#%%[Markdown] =================================================================
# section 0: imports
#   (1) imports
#
# =============================================================================

# %% (1) imports
import json
import os
from functools import wraps
import time
import requests
from urllib3.exceptions import MaxRetryError, NewConnectionError
from requests.exceptions import ConnectionError
import msal

#%%[Markdown] =================================================================
#
# section 1: helper functions, decorators and classes
#   (1) retry decorator
#   (2) special error class for retry
#   (3) get new headers / token
#
# =============================================================================

#%% (1) retry decorator
def retry(exceptions, total_tries=4, initial_wait=30, backoff_factor=2):
    """Call the decorated function that applies an exponential backoff.

    based on https://gist.github.com/FBosler/be10229aba491a8c912e3a1543bbc74e

    Args:
        exceptions (object): Exception(s) that trigger a retry, can be a tuple
        total_tries (int): Total tries
        initial_wait (int): Time to first retry
        backoff_factor (numeric): Backoff multiplier (e.g. value of 2 will double the delay each
            retry).
    """
    def retry_decorator(func):
        @wraps(func)
        def func_with_retries(*args, **kwargs):
            _tries, _delay = total_tries + 1, initial_wait
            while _tries > 1:
                try:
                    return func(*args, **kwargs)
                except exceptions as handled_exception:
                    _tries -= 1
                    if _tries == 1:
                        print(f"""Function: {func.__name__}\n
                                  Failed after {total_tries} tries.\n""")
                        raise
                    print(f"""Function: {func.__name__}\n
                              Exception: {handled_exception}\n
                              Retrying in {_delay} seconds!""")
                    time.sleep(_delay)
                    _delay *= backoff_factor

        return func_with_retries
    return retry_decorator

# %% (2) special error class for retry
class RetryableError(Exception):
    """Raise special Exception for the retry decorator."""

    # do nothing

# %% (3) get new headers / token
@retry(RetryableError, total_tries=6, initial_wait=10, backoff_factor=2.0)
def get_headers(ms_credentials):
    """Create ready-to-use headers for request with msal token.

    args:
        ms_credentials (json) : microsoft credentials to create token

    return:
        headers (json) : ready-to-use headers for request to onedrive api
    """
    # prepare credentials
    if isinstance(ms_credentials, str):
        ms_credentials = json.loads(ms_credentials)

    # check whether all credentials provided
    if ms_credentials.get("client_id") is None:
        raise Exception("client_id missing")
    elif ms_credentials.get("tenant_id") is None:
        raise Exception("tenant_id missing")
    elif ms_credentials.get("user") is None:
        raise Exception("user missing")
    elif ms_credentials.get("key") is None:
        raise Exception("key missing")
    else: pass

    # set up required variable
    scopes = ['Sites.ReadWrite.All','Files.ReadWrite.All']
    authority = f"https://login.microsoftonline.com/{ms_credentials.get('tenant_id')}"

    # create client application
    try:
        app = msal.ConfidentialClientApplication(
            ms_credentials.get("client_id"),
            authority=authority)

        # get token
        token = app.acquire_token_by_username_password(
            ms_credentials.get("user"),
            ms_credentials.get("key"),
            scopes)

        # check result
        if token.get('access_token') is None:
            raise Exception("no token created")

    except (ConnectionError, MaxRetryError, NewConnectionError) as temporary_issues:
        # catching the specific exception didn't work, hence all exceptions are
        #   caught and retried
        raise RetryableError from temporary_issues

    # create headers
    headers = {"Authorization": f"Bearer {token['access_token']}"}

    return headers

# %% [Markdown]# ===============================================================
#
# Section 2: functions
#   (1) upload to onedrive
#
# =============================================================================

# %% (1) upload to onedrive from temp path
def upload_temp_to_onedrive(drive_id, folder_id, upload_name, inputpath, ms_credentials,
    on_conflict = "replace"):
    """Upload file to onedrive loaded from a local path.

    args:
        drive_id (str) : the id of the drive the file should be uploaded to
        folder_id (str) : the id of the folder the file should be uploaded to
        upload_name (str) : the intended name of the target file
        filepath (str) : the path to the input file
        ms_credentials (json) : the credentials to get the upload token
        on_conflict (str) : how to handle file conflict situations (options: rename,
            fail, replace)

    return:
        no return
    """
    # checks
    if on_conflict not in ["rename", "fail", "replace"]:
        raise Exception(f"on conflict {on_conflict} arg unknown")
    if not os.path.isfile(inputpath):
        raise Exception(f"file {inputpath} does not exists")

    # create variables
    endpoint = "https://graph.microsoft.com/v1.0"
    destination = f'{endpoint}/drives/{drive_id}/items/{folder_id}:/{upload_name}:createUploadSession'

    # get headers
    headers = get_headers(ms_credentials=ms_credentials)

    # create upload session
    upload_session = requests.post(
        url=destination,
        headers=headers,
        json={
            "item": {
                "@microsoft.graph.conflictBehavior": on_conflict,
            }
        })
    if upload_session.status_code not in [200, 201]:
        raise Exception("could not establish upload session")

    # upload file
    with open(inputpath, 'rb') as file:
        # get size and split in chunks
        total_file_size = os.path.getsize(inputpath)
        chunk_size = 327680
        chunk_number = total_file_size//chunk_size # the number of full chunks required to upload
        chunk_leftover = total_file_size - chunk_size * chunk_number # the last chuck to upload
        i = 0
        # loop through chunks
        while True:
            chunk_data = file.read(chunk_size)
            start_index = i*chunk_size
            end_index = start_index + chunk_size
            # If end of file, break
            if not chunk_data:
                break
            if i == chunk_number:
                end_index = start_index + chunk_leftover
            # Setting the header with the appropriate chunk data location in the file
            session_headers = {
                'Content-Length':f'{chunk_size}',
                'Content-Range': f'bytes {start_index}-{end_index-1}/{total_file_size}'}
            # Upload one chunk at a time
            chunk_data_upload = requests.put(
                upload_session.json()['uploadUrl'],
                data=chunk_data, headers=session_headers)
            print(f"upload {i+1}/{chunk_number + 1} : {chunk_data_upload}")
            i += 1
