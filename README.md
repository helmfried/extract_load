# Purpose

Includes helper functions for extracting and loading data in simplified versions (e.g. from/to DataFrame, from/to csv) for various data sources:

* postgresql DB
* mysql DB
* API
* onedrive
* gdrive

Intentionally based on hierarchy of wrappers:


**extraction**

1. wrap a transformation to DataFrame around extract from data source
2. wrap a file writing to csv around DataFrame


**Loading**

1. wrap a transformation from DataFrame to the load into the data store
2. wrap a file reading around transformation functions 