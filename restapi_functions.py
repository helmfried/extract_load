"""Wrap around request functions to simplify often used additions.

(initial) GET request to df, retry decorator, [unfinished] paginated
(2021-09-09) POST request
(2021-09-14) paginated works
(2021-09-27) added body to call
(2021-09-30) pagination by list
"""

# %% [Markdown]# ===============================================================
# 
# Section 0: imports & parameters
#   (1) package imports
# 
# =============================================================================

# %% (1) package imports
import requests
from functools import wraps
import time
import urllib
import pandas as pd

#%%[Markdown] =================================================================
#
# section 1: helper functions, decorators and classes
#   (1) retry decorator
#   (2) special error class for retry
#
# =============================================================================

#%% (1) retry decorator
def retry(exceptions, total_tries=4, initial_wait=30, backoff_factor=2):
    """Call the decorated function that applies an exponential backoff.
    
    based on https://gist.github.com/FBosler/be10229aba491a8c912e3a1543bbc74e

    Args:
        exceptions (object): Exception(s) that trigger a retry, can be a tuple
        total_tries (int): Total tries
        initial_wait (int): Time to first retry
        backoff_factor (numeric): Backoff multiplier (e.g. value of 2 will double the delay each 
            retry).
    """
    def retry_decorator(f):
        @wraps(f)
        def func_with_retries(*args, **kwargs):
            _tries, _delay = total_tries + 1, initial_wait
            while _tries > 1:
                try:
                    return f(*args, **kwargs)
                except exceptions as e:
                    _tries -= 1
                    if _tries == 1:
                        print(f"""Function: {f.__name__}\n
                                  Failed after {total_tries} tries.\n""")
                        raise
                    print(f"""Function: {f.__name__}\n
                              Exception: {e}\n
                              Retrying in {_delay} seconds!""")
                    time.sleep(_delay)
                    _delay *= backoff_factor

        return func_with_retries
    return retry_decorator

# %% (2) special error class for retry
class RetryableError(Exception):
    """Raise special Exception for the retry decorator."""
    
    # do nothing

#%%[Markdown] =================================================================
# section 2: functions
#   (1) request to df
#   (2) paginated requests to df
#
# =============================================================================

# %% (1) request to df
@retry(RetryableError, total_tries=5, initial_wait=5, backoff_factor=1.5)
def single_request_to_df(url, headers=None, params={}, sub_key=None, session=None, 
    call_method="get", body = None):
    """GET data from API and transform to dataframe.

    args:
        url (str) : url of the API
        headers (dict) : the required authorization for the api
        params (dict) : additional parameters for the get request
        sub_key (str) : key of nested API repsonse.
        session (object of requests) : an session of the requests package that is
            provided for continuous requests
        call_method (str) : how to call
        body (dict) : additional content for call

    exceptions:
        Raises Exception of wrong Authorization

    return:
        (df) : normalized dataframe of request content
    """
    # open session if not provided
    session_provided = session is not None
    if not session_provided: 
        session = requests.Session()
    # adapt params (unfortunately, the encoding does not work for list types)
    if not any([isinstance(params[k], list)  for k in params.keys()]):
        params = urllib.parse.urlencode(
            params,
            quote_via=urllib.parse.quote
        )
    try :
        if call_method == "get":
            payload = session.get(url, params = params, headers=headers)
        elif call_method == "post":
            payload = session.post(url, params = params, headers=headers, json = body)
        else:
            raise Exception(f"{call_method} call_method unknown")
    except (ConnectionError, requests.HTTPError, requests.TooManyRedirects) as temporary_issues:
        raise RetryableError from temporary_issues
    # Raise Exception of wrong Authorization
    if payload.status_code in [400, 401]:
        raise Exception(f"""Status Code {payload.status_code}: something went wrong
            -------------------------------------------------
            400 -  bad request
            401 - wrong authentication
            -------------------------------------------------
            msg:
            {payload.content}""")
    # close session if explicitly created
    if not session_provided:
        session.close()
    # return data
    # TODO: requires test of whether the sub_key exists and is not none (e.g. empty pages)  
    if sub_key is None:
        return pd.json_normalize(payload.json(), max_level=1)
    else:
        return pd.json_normalize(payload.json()[sub_key], max_level=1)

# %% (2) paginated requests to df
def paginated_request_to_df(url, page_parameter, page_initial = 0, page_limit = 0, 
    headers=None, params={}, sub_key=None, rate_limit = 200, escape_condition = None,
    paging_mode = "offset", body = None, call_method = "get", pagination_params = None):
    """GET paginated data and transform to dataframe.

    Function assumes simply nested (1 level max) data, headers and parames provided 
        as dict (except the parameter of the offset / startat parameter required for 
        pagination).

    args:
        url (str) : url of the API
        page_parameter (str) : name of the parameter that offsets the entry
            to start fetching data, which is required for pagination.
        page_initial (int) : number of data entries to skip. As opposed to the 
            page_limit, this does refer to the entries and does not take the default
            page size of the API into account.
        page_limit (int) : max number of pages to fetch, set to 0 for no limit. Will
            fetch the default (probably unknown) page size of the API page_limit number
            of times.
        headers (dict) : the required authorization for the api
        params (dict) : additional parameters for the get request
        rate_limit (int) : maximum number of calls per minute
        exception_condition (str) : a query condition for the df returned from the data, 
            which will stop the data pull 
        paging_mode (str) : increase by "offset" (returned results per call) or by "page" 
            (increase by 1 after each call) or "list" (get list of values to call divided by 
            page_size)
        call_method (str) : whether "post" or "get"
        body (dict) : additional content for call
        pagination_params (dict) : params use for pagination should contain a call_list that 
            carries the ids to call for the provided page_parameter, and a 'split_size' by which the
            call_list will be split.

    exceptions:
        Raises Exception on wrong data type of parameters

    return:
        df (df) : dataframe version of fetched data
    """
    # Raise Exception on wrong data type of parameters
    if type(params) != dict:
        raise f"""Provided params in wrong format: {type(params)}"""
    # Raise Exception on wrong paging mode
    if paging_mode not in ["offset", "page", "list"]:
        raise Exception(f"{paging_mode} is no accepted paging mode")
    # on list paging, split call_list into multiple parts
    if paging_mode == "list":
        if pagination_params.get("call_list") is None: raise Exception("no call_list")
        elif pagination_params.get("split_size") is None: raise Exception("no call_list")
        else:
            chunks = [
                pagination_params.get("call_list")[i:i + pagination_params.get("split_size")] 
                for i in range(0, len(pagination_params.get("call_list")), pagination_params.get("split_size"))]
            page_initial = chunks[0]
    # set up help variables
    offset = page_initial
    bucket = pd.DataFrame()
    # go_on = True
    call = 1
    session = requests.Session()
    rate_counter_time = pd.to_datetime("now").floor("1Min")
    rate_counter = 0
    # loop through pages
    try:
        while True:
            # message
            if paging_mode in ["offset", "page"]: 
                print(f"call {call} : fetching starting at {offset}")
            elif paging_mode in ["list"]:
                print(f"call {call} : fetching starting at {offset[0]} to {offset[-1]}")
            # define parameters for pagination
            request_params = {
                **{page_parameter : offset},
                **params
                }
            # use individual function
            temp = single_request_to_df(
                url, 
                headers=headers, 
                params=request_params,
                sub_key=sub_key,
                session=session,
                body = body,
                call_method = call_method
            )
            bucket = bucket.append(temp, ignore_index=True)
            # evaluate continuation
            if call >= page_limit or page_limit == 0:
                print("calling ended: provided page limit reached")
                break
            if len(temp) == 0 and paging_mode not in ["list"]:
                print("calling ended: no more results on last called page")
                break
            # increase page
            if paging_mode == "offset":
                offset += len(temp)
            elif paging_mode == "page":
                offset += 1
            elif paging_mode == "list":
                if call < len(chunks):
                    print(call)
                    print(len(chunks))
                    offset = chunks[call]
                else:
                    print("calling ended: no more page ids to call")
                    break
            # count calls
            call += 1
            # enforce rate limit (very simple way)
            if rate_counter_time == pd.to_datetime("now").floor("1Min"):
                rate_counter += 1
                print(f"rate count : {rate_counter}")
                if rate_counter >= rate_limit:
                    time.sleep((pd.to_datetime("now").ceil("1Min") - pd.to_datetime("now").floor("1S")).seconds)
            else:
                rate_counter_time = pd.to_datetime("now").floor("1Min")
                rate_counter = 0
            # check escape condition
            # TODO: remains untested!
            if escape_condition is not None and len(bucket.query(escape_condition, engine = "python")) > 0:
                print("calling ended: escape condition triggered")
                break
    except Exception as e:
        print(e)
    finally:
        # try except finally combination to get partial results on error
        session.close()
        return bucket

# %%
