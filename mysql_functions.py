"""Helper functions for simplified query execution on Wunder databases.

In the origin form, this file is oriented towards a single database given with the dotenv file.
(2021-09-17) introduced connect_args and additional engine arguments to mysql func
"""

#%%[Markdown] =================================================================
# section 0: imports
#   (1) imports
#   (2) register data types for psycog2
#   (3) get credentials
#
# =============================================================================

#%% (1) imports
import pandas as pd
import sqlalchemy
from dotenv import load_dotenv
import os
import logging
from sqlalchemy.pool import QueuePool

#%% register data types
# NOTE: not necessary, no writing rights on Wunder!

#%% (3) get credentials
load_dotenv()
user = os.getenv("MYSQL_USER")
key = os.getenv("MYSQL_KEY")
host = os.getenv("MYSQL_HOST")
db = os.getenv("MYSQL_NAME")
auth = (host, db, user, key)

#%%[Markdown] =================================================================
# section 1: post functions
#   NO Writing Rights on an mysql db so far
#
# =============================================================================

#%%[Markdown] =================================================================
# section 2: get / query functions
#   (1) generic query statement with return (sqlalchemy version)
#
# =============================================================================

#%% (1) generic query statement with return
def get_generic_mysql(statement, auth=auth, params_string = ""):
    """Query based on provided statement.
    
    params
        auth: Connection credentials
        statement: query to select data
        params_string: parameters for the connection, already as string of 'key=value'
    
    return
        df : fetched data
    """
    conn = None
    df = None
    if params_string != "":
        params_string = f"?{params_string}"
    try:
        engine = sqlalchemy.create_engine(
            f"mysql://{auth[2]}:{auth[3]}@{auth[0]}/{auth[1]}{params_string}",
            connect_args={
                "connect_timeout": 1500,
                },
            poolclass = QueuePool, 
            pool_pre_ping = True,
            pool_size = 10,
            pool_recycle=3600,
            pool_timeout = 900,
        )
        conn = engine.connect()
        df =  pd.read_sql_query(statement, conn)
    except Exception as e:
        print(e)
        logging.error(e)
        raise Exception("could not load data")
    finally:
        if conn:
            conn.close()
    return df
