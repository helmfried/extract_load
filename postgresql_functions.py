"""Helper functions for simplified population of pseudoDHW databases.

In the origin form, this file is oriented towards a single database given with the dotenv file.

(2021-07-23) This version contains a return of ids
"""

#%%[Markdown] =================================================================
# section 0: imports
#   (1) imports
#   (2) register data types for psycog2
#   (3) get credentials
#
# =============================================================================

#%% imports
import pandas as pd
import psycopg2
import psycopg2.extras as extras
import numpy as np
from psycopg2.extensions import register_adapter, AsIs
from dotenv import load_dotenv
import os
import logging

#%% register data types
register_adapter(np.float64, lambda a: AsIs(a))
register_adapter(np.int64, lambda a: AsIs(a))
register_adapter(np.bool_, lambda a: AsIs(a))

#%% (3) get credentials
load_dotenv()
user = os.getenv("DB_USER")
key = os.getenv("DB_KEY")
host = os.getenv("DB_HOST")
db = os.getenv("DB_NAME")
auth = (host, db, user, key)

#%%[Markdown] =================================================================
# section 1: post functions
#   (1) generic writing statement without return
#   (2) upsert writing into table without statement
#
# =============================================================================

#%% (1) generic statement without return
def post_generic_postgreSQL(statement, auth=auth):
    """Execute a generic sql command.

    Use for create table, create views!

    params
        auch: database connection credentials 
        statement: an sql statet statement
    
    return
        None
    """
    conn = None
    c = None
    try:
        conn = psycopg2.connect(
            host=auth[0],
            database=auth[1],
            user=auth[2],
            password=auth[3]
        )
        c = conn.cursor()
        c.execute(statement)
        conn.commit()
    except psycopg2.Error as e:
        print(e)
        logging.error(e)
        raise Exception("could not post data")
    finally:
        if conn:
            conn.close()
        if c:
            c.close()

#%% (2) upsert writing into table without statement
def post_upsert_postgreSQL(table, df, auth=auth, bulk_size = 1000, cols_to_return = None):
    """Insert into a table or update rows (upsert) into table populated by df.
    
    params
        auth: Connection credentials
        table: the table to insert values into
        df: a dataframe to populate the table
        bulk_size: how many commits at a time
        cols_to_return (str or list) : specific columns to return, e.g. id col
    
    return
        None
    """
    # create empty variables for better control
    conn = None
    c = None
    
    # corrective actions for writing
    df = df.replace({np.nan: None})

    # create query
    # ...as opposed to sqlite, one placeholder is enough, no need for exact number
    # ... such as: vals = ", ".join(len(df.columns) * ["%s"])
    cols = ", ".join(list(df.columns))

    # if this is not an f string, double %% required
    insert_sql  = f"""INSERT INTO {table}
        ({cols}) VALUES %s
        ON CONFLICT on constraint {table}_pkey
        DO UPDATE SET ({cols}) = ({', '.join(["EXCLUDED." + x for x in list(df.columns)])})"""
    if cols_to_return is not None:
        if not isinstance(cols_to_return, list): cols_to_return = [cols_to_return]
        insert_sql = insert_sql + f"""
        RETURNING {', '.join(cols_to_return)}"""

    # write data
    print(f"executing: \n{insert_sql}")
    try:
        conn = psycopg2.connect(
            host=auth[0],
            database=auth[1],
            user=auth[2],
            password=auth[3]
        )
        c = conn.cursor()
        committed = pd.DataFrame()
        for idx, group in df.groupby(np.arange(len(df))//bulk_size):
            # status
            print(f"executing {idx + 1}/{(len(df)//bulk_size) + 1}")        
            # send data and commit
            extras.execute_values(
                c,
                insert_sql, 
                group.to_records(index=False))
            conn.commit()
            # get the RETURNING values
            if cols_to_return is not None:
                committed = committed.append(c.fetchall(), ignore_index=True)
    except (psycopg2.Error, psycopg2.DatabaseError, ValueError) as e:
        print(e)
        logging.error(e)
        conn.rollback()
        raise Exception("could not post data")
    finally:
        if conn:
            conn.close()
        if c:
            c.close()
        if cols_to_return is not None:
            return committed
        else:
            pass

#%%[Markdown] =================================================================
# section 2: get / query functions
#   (1) generic statement  without return (POST)
#
# =============================================================================


#%% (1) generic query statement with return
def get_generic_postgreSQL(statement, auth=auth):
    """Query based on provided statement.
    
    params
        auth: Connection credentials
        statement: query to select data
    
    return
        df : fetched data
    """
    conn = None
    df = None
    try:
        conn = psycopg2.connect(
            host=auth[0],
            database=auth[1],
            user=auth[2],
            password=auth[3]
        )
        df = pd.read_sql_query(statement, conn)
    except psycopg2.Error as e:
        print(e)
        logging.error(e)
        raise Exception("could not post data")
    finally:
        if conn:
            conn.close()
    return df

# %%
